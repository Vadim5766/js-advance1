class Employee {
  constructor(name, age, salery) {
    this.name = name;
    this.age = age;
    this.salery = salery;
  }
  set name(value) {
    this._name = value;
  }
  get name() {
    return this._name;
  }
  set age(value) {
    this._age = value;
  }
  get age() {
    return this._age;
  }
  set salery(value) {
    this._salery = value;
  }
  get salery() {
    return this._salery;
  }
}

class Programmer extends Employee {
  constructor(name, age, salery, lang) {
    super(name, age, salery);
    this.lang = lang;
  }
  set salery(value) {
    this._salery = value;
  }
  get salery() {
    return this._salery * 3;
  }
}

const employee = new Employee("Uasya", 23, 1000);
const program1 = new Programmer("Masha", 20, 1300, "eng");
const program2 = new Programmer("Ira", 54, 800, "ukr");
const program3 = new Programmer("Vadik", 25, 1500, "ukr");
console.log(employee);
console.log(program1);
console.log(program2.salery);
console.log(program3);
console.log(program3.salery);
